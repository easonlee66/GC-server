//头文件的东西，请根据自己的安装目录设置
#include <gtk-2.0/gtk/gtk.h>
#include <gtk-2.0/gdk/gdk.h>
 
#include <gtk-2.0/gtk/gtkdialog.h>
#include <gtk-2.0/gtk/gtktypeutils.h>
#include <gtk-2.0/gtk/gtkmain.h>
#include <gtk-2.0/gtk/gtkstyle.h>
#include <gtk-2.0/gtk/gtkmessagedialog.h>
#include <iostream>
#include <gtk-2.0/gtk/gtkwindow.h>
using namespace std;
 
 
void close_app(GtkWidget *widget, gpointer data) {
    gtk_main_quit();
}
//使用gtk编译参数加上 `pkg-config --cflags --libs gtk+-2.0`
 
int main(int argc, char** argv) {
 
    GtkWidget *dialog;
    //初始化GTK环境    
    gtk_init(&argc, &argv);
    dialog = gtk_message_dialog_new(NULL,
            GTK_DIALOG_DESTROY_WITH_PARENT,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_OK, argv[1], "系统提示");
    gtk_window_set_title(GTK_WINDOW(dialog), "系统提示");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    //绑定信号函数,点击退出时执行的操作 
    //g_signal_connect(GTK_OBJECT(dialog), "destroy", GTK_SIGNAL_FUNC(close_app), NULL);
    //gtk_widget_show_all(dialog);
    //gtk_main();
    return 0;
}
 